import {Component, OnInit,} from '@angular/core';
import {MarsupilamiServicesService} from "../marsupilami-services.service";

import {Router} from "@angular/router";

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {
  constructor(private service: MarsupilamiServicesService,private router: Router) { }

  error='';
model:any=[];

  ngOnInit() {
  }
  //Executed onSubmit the form connection,
   VerifyConnection(){
   this.service.Verify_Connection_server(this.model.email,this.model.password)
     .subscribe(response => {
       localStorage.setItem('token', response.token);
         this.router.navigate(['Home']);
     },err=>{
       if (err.status === 404) {
         this.error=err.error.message;
       }
     });
  }
}
