import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";


@Injectable({
  providedIn: 'root'
})
export class MarsupilamiServicesService {

  constructor(private httpClient: HttpClient, private router: Router) { }

  //POST request : To verify user authentication using email and password
  Verify_Connection_server(email, password){
    return this.httpClient.post<any>('http://localhost:3000/connection-verify', {
      "email": email,
      "password": password
    });
  }
// POST request : To add user to mongoDB
  save_data_marsupilami_server(race,famille,nourriture,age,email,password,password_2) {
    return this.httpClient.post<any>('http://localhost:3000/post-data-marsupilami', {
      "race": race,
      "famille": famille,
      "nourriture": nourriture,
      "age": age,
      "email":email,
      "password": password,
      "password_2": password_2
    });
  }
//GET request : To retrieve user Data from mongoDB
  ge_Data_Profil(){
    return this.httpClient.get<any>('http://localhost:3000/myProfil');
  }
// Get request : To delete a friend form friends list
  delete_Friend_service(id){

    return this.httpClient.get<any>('http://localhost:3000/deleteFriend/'+id);
  }
// Get request : To add a friend form friends list
  add_Friend_service(id){

    return this.httpClient.get<any>('http://localhost:3000/addFriend/'+id);

  }
// Get request : To retrieve user Data to be updated
  ge_Data_Profil_To_Update(){
    return this.httpClient.get<any>('http://localhost:3000/Modifier_Information');
  }
  //POST request : To save changes, update users Data
  Changes_save_data(race,famille,nourriture,age,){
    return this.httpClient.post<any>('http://localhost:3000/saveChanges', {
      "race": race,
      "famille": famille,
      "nourriture": nourriture,
      "age": age
    });

  }
//POST request : To get the  ID of friend added by connected user.
  ge_ID_Friend_Added(email){
    return this.httpClient.post<any>('http://localhost:3000/FriendID',{"email":email});
  }

//Boolean : To verify the existence of token in the localStorage
  LoggedIn(){
    return !!localStorage.getItem('token');
  }

  // Returns the token from  the localStorage
  getToken(){
    return localStorage.getItem('token');
  }
//To logOut, delete the token from LocalStorage
  LogOut(){
    localStorage.removeItem('token');
    this.router.navigate(['Home']);
  }

}
