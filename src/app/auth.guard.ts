import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {MarsupilamiServicesService} from "./marsupilami-services.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private service: MarsupilamiServicesService, private router: Router) { }

  canActivate(): boolean {
    if(this.service.LoggedIn()){
      return true;
    }else{
      this.router.navigate(['Connection']);
      return false;
    }
  }
}
