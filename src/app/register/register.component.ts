import { Component, OnInit} from '@angular/core';
import {MarsupilamiServicesService} from "../marsupilami-services.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any={};
  error='';
  password_2='';
  constructor(private service: MarsupilamiServicesService, private router: Router) { }

  ngOnInit() {
  }
//onSubmit the from either form registration or for adding a friend
  onSubmit(){

    this.service.save_data_marsupilami_server(this.model.race,this.model.famille,this.model.nourriture,this.model.age,this.model.email,this.model.password,this.model.password_2)
      .subscribe(response => {
        //To verify if the user will do a registration or will add friend
        //if the user is not connected so
        if(!this.service.LoggedIn()){
          localStorage.setItem('token', response.token);
          this.router.navigate(['Home']);
          //else he is adding a friend
        }else{

          this.service.ge_ID_Friend_Added(this.model.email).subscribe(resp=>{
            this.service.add_Friend_service(resp.idFriend).subscribe(res=>{
              this.router.navigateByUrl('Home', {skipLocationChange: true}).then(()=>
                this.router.navigate(["Profil"]));

            });
          })

        }


      },err=>{
        if (err.status === 404) {
          this.error=err.error.message;

        }


      });



  }

}
