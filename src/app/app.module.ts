import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MarsupilamiServicesService} from "./marsupilami-services.service";
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ConnectionComponent } from './connection/connection.component';
import { RegisterComponent } from './register/register.component';
import { MyProfilComponent } from './my-profil/my-profil.component';
import {RouterModule} from "@angular/router";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { UpdateProfilComponent } from './update-profil/update-profil.component';
import {AuthGuard} from "./auth.guard";
import {TokenInterceptorService} from "./token-interceptor.service";
import {ConfirmEqualValidatorDirective} from "./password-validator.directive";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    ConnectionComponent,
    RegisterComponent,
    MyProfilComponent,
    UpdateProfilComponent,
    ConfirmEqualValidatorDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,


    FormsModule,
    RouterModule.forRoot([
      {
        path : '',
        component :HomeComponent
      },
      {
        path : 'Home',
        component :HomeComponent
      },
      {
        path : 'Register',
        component :RegisterComponent
      },
      {
        path : 'Profil/Register',
        component :RegisterComponent
      },


      {
        path : 'Connection',
        component :ConnectionComponent
      },
      {
        path : 'Profil',
        component :MyProfilComponent,
        canActivate:[AuthGuard]
      },
      {
        path : 'Profil/UpdateProfil',
        component :UpdateProfilComponent,
        canActivate:[AuthGuard]
      }
    ])
  ],
  providers: [MarsupilamiServicesService,AuthGuard,{
    provide : HTTP_INTERCEPTORS,
    useClass:TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
