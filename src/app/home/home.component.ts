import { Component, OnInit } from '@angular/core';
import {MarsupilamiServicesService} from "../marsupilami-services.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private service: MarsupilamiServicesService) { }

  ngOnInit() {
  }

}
