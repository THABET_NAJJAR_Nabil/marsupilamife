import { TestBed, inject } from '@angular/core/testing';


import { MarsupilamiServicesService } from './marsupilami-services.service';
describe('MarsupilamiServicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarsupilamiServicesService]
    });
  });

  it('should be created', inject([MarsupilamiServicesService], (service: MarsupilamiServicesService) => {
    expect(service).toBeTruthy();
  }));
});
